'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

describe('index extension table tests', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup()
    contentCatalog = r.contentCatalog
    file = r.file
  })

  function checkDoc (doc, count, matches) {
    expect(doc).to.not.equal(null)
    expect(doc.blocks.length).to.eq(1)
    const block = doc.blocks[0]
    expect(block.context).to.eq('table')
    expect(block.rows).to.not.equal(null)
    expect(block.rows.body).to.not.equal(null)
    expect(block.rows.body.length).to.equal(count)
    block.rows.body.forEach((row, i) => {
      expect(row.length).to.equal(matches.length)
      row.forEach((cell, j) => {
        expect(cell.context).to.equal('cell')
        expect(cell.text).to.match(matches[j])
      })
    })
  }

  function subMatch (match, matches) {
    return matches.map((m) => m === '$xref' ? match : m)
  }

  ;[{
    page: (filter) => `[cols=2*,options="header"]
|===
| link
| attribute
|===

indexTable::[cells="$xref,description",${filter}]
`,
    matches: ['$xref', /description ./],
  },
  {
    page: (filter) => `[cols=2*,options="header"]
|===
| attribute
| link
|===

indexTable::[cells="description,$xref",${filter}]
`,
    matches: [/description ./, '$xref'],
  },
  {
    page: (filter) => `[cols=3*,options="header"]
|===
| attribute
| link
| attribute
|===

indexTable::[cells="description,$xref,description",${filter}]
`,
    matches: [/description ./, '$xref', /description ./],
  },
  {
    page: (filter) => `[cols=3*,options="header"]
|===
| attribute
| link
| trimmed attribute
|===

indexTable::[cells="description,$xref,=description.slice(12)",${filter}]
`,
    matches: [/description ./, '$xref', /[0-5]/],
  },
  {
    page: (filter) => `[cols=3*,options="header"]
|===
| attribute
| link
| trimmed attribute
|===

indexTable::[cells="description,$xref,=\`*$\{mod2}* and $\{mod3}\`",${filter}]
`,
    matches: [/description ./, '$xref', /\*[01]\* and [012]/],
  },
  {
    page: (filter) => `[cols=3*,options="header"]
|===
| attribute
| link
| trimmed attribute
|===

indexTable::[cells="description,$xref,=\`$\{$xref} and $\{mod3}\`",${filter}]
`,
    matches: [/description ./, '$xref', /xref:.*page.\.adoc\[] and [012]/],
  },
  ].forEach(({ page, matches }) => {
    describe(`match: ${matches}`, () => {
      it('no filter test', () => {
        file.contents = Buffer.from(page(''))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 10, subMatch(/xref:1\.0@c1:ROOT:(topic\/)?page.\.adoc/, matches))
      })

      it('component * filter test', () => {
        file.contents = Buffer.from(page('component=*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 20, subMatch(/xref:1\.0@c.:ROOT:(topic\/)?page.\.adoc/, matches))
      })

      it('component c2 filter test', () => {
        file.contents = Buffer.from(page('component=c2'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 10, subMatch(/xref:1\.0@c2:ROOT:(topic\/)?page.\.adoc/, matches))
      })

      it('version * filter test', () => {
        file.contents = Buffer.from(page('version=*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 20, subMatch(/xref:(1\.0|master)@c1:ROOT:(topic\/)?page.\.adoc/, matches))
      })

      it('version master filter test', () => {
        file.contents = Buffer.from(page('version=master'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 10, subMatch(/xref:master@c1:ROOT:(topic\/)?page.\.adoc/, matches))
      })

      it('module * filter test', () => {
        file.contents = Buffer.from(page('module=*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 20, subMatch(/xref:1\.0@c1:(ROOT|module):(topic\/)?page.\.adoc/, matches))
      })

      it('module module filter test', () => {
        file.contents = Buffer.from(page('module=module'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 10, subMatch(/xref:1\.0@c1:module:(topic\/)?page.\.adoc/, matches))
      })

      it('relative topic filter test', () => {
        file.contents = Buffer.from(page('relative=topic/**'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 5, subMatch(/xref:1\.0@c1:ROOT:topic\/page.\.adoc/, matches))
      })
    })
  })
})
