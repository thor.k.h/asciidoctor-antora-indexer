'use strict'

const classifyContent = require('@antora/content-classifier')

module.exports = (normal = true, nested = false, nameSeparator = '') => {
  const primarySiteUrl = 'https://example.com'
  const path = __dirname
  const contentCatalog = classifyContent({ site: {} }, [])
  if (normal) {
    ;['c1', 'c2'].forEach((component) => {
      ['1.0', 'master'].forEach((version) => {
        contentCatalog.registerComponentVersion(component, version, {
          displayVersion: version,
          title: component + ' title',
        })
        ;['ROOT', 'module'].forEach((module) => {
          ;['', 'topic/'].forEach((topic) => {
            ;[1, 2, 3, 4, 5].forEach((id) => {
              const file = contentCatalog.addFile({
                asciidoc: {
                  attributes: {
                    doctitle: `${component} ${version} ${module} ${topic} ${id}`,
                    description: `description ${id}`,
                    'the-description': `description ${id}`,
                    mod2: id % 2,
                    mod3: id % 3,
                  },
                },
                origin: { site: primarySiteUrl },
                out: undefined,
                pub: {
                  url: `${primarySiteUrl}'/c/1.0/${module === 'ROOT' ? '' : 'module/'}${topic}page${id}.html`,
                  moduleRootPath: '',
                },
                src: {
                  component,
                  version,
                  module,
                  relative: `${topic}${nameSeparator}page${nameSeparator}${id}.adoc`,
                  family: 'page',
                },
                path,
              })
              if (id % 2 === 0) {
                file.asciidoc.attributes.even = undefined
              }
            })
          })
        })
      })
    })
  }
  if (nested) {
    const [component, version, module, topic] = ['nested', '1.0', 'ROOT', '']
    ;['I', 'II', 'III'].forEach((l0) => {
      ;['A', 'B', 'C'].forEach((l1) => {
        ;['1', '2', '3'].forEach((l2) => {
          ;[1, 2, 3, 4, 5].forEach((id) => {
            const name = `${l0}.${l1}.${l2}.${id}`
            const file = contentCatalog.addFile({
              asciidoc: {
                attributes: {
                  doctitle: `${component} ${version} ${module} ${topic} ${name}`,
                  description: `description ${name}`,
                  l0,
                  l1,
                  l2,
                  id,
                  mod2: id % 2,
                  mod3: id % 3,
                },
              },
              origin: { site: primarySiteUrl },
              out: undefined,
              pub: {
                url: `${primarySiteUrl}'/c/1.0/${module === 'ROOT' ? '' : 'module/'}${topic}page${name}.html`,
                moduleRootPath: '',
              },
              src: {
                component,
                version,
                module,
                relative: `${topic}page${name}.adoc`,
                family: 'page',
              },
              path,
            })
            if (id % 2 === 0) {
              file.asciidoc.attributes.even = undefined
            }
          })
        })
      })
    })
  }

  const baseFile = {
    asciidoc: { xreftext: 'doctitle' },
    origin: { site: primarySiteUrl },
    out: undefined,
    pub: {
      url: primarySiteUrl + '/c1/1.0/index.html',
      moduleRootPath: '',
    },
    src: {
      component: 'c1',
      version: '1.0',
      module: 'ROOT',
      relative: 'index.adoc',
      family: 'page',
    },
    path,
  }
  const file = contentCatalog.addFile(baseFile)
  return { contentCatalog, file, primarySiteUrl, path }
}
