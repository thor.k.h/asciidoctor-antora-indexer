'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

describe('index extension template tests with include', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup(false, false)
    contentCatalog = r.contentCatalog
    ;['', 'topic/'].forEach((topic) => {
      ;[1, 2, 3, 4, 5].forEach((id) => {
        contentCatalog.addFile({
          origin: { site: r.primarySiteUrl },
          out: undefined,
          src: {
            component: 'c1',
            version: 'master',
            module: 'm1',
            family: 'partial',
            relative: `${topic}page${id}.adoc`,
          },
          contents: Buffer.from(`topic: ${topic} id: ${id}:: explanation\n`),
          path: r.path,
        })
      })
    })
    file = r.file
  })

  function checkDoc (doc, count, match) {
    expect(doc).to.not.equal(null)
    expect(doc.blocks.length).to.eq(count)
    doc.blocks.forEach((block, i) => {
      expect(block.context).to.eq('dlist')
    })
  }

  ;[
    {
      page: (filter) => `

[indexTemplate,'resourceid',family=partial,version=master,module=m1]
----
\\include::{resourceid}[]
----
`,
    },
  ].forEach(({ page, matches }) => {
    describe(`match: ${matches}`, () => {
      it('no filter test', () => {
        file.contents = Buffer.from(page(''))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 1, matches)
        expect(doc.convert()).to.equal(`<div class="dlist">
<dl>
<dt class="hdlist1">topic:  id: 1</dt>
<dd>
<p>explanation</p>
</dd>
<dt class="hdlist1">topic:  id: 2</dt>
<dd>
<p>explanation</p>
</dd>
<dt class="hdlist1">topic:  id: 3</dt>
<dd>
<p>explanation</p>
</dd>
<dt class="hdlist1">topic:  id: 4</dt>
<dd>
<p>explanation</p>
</dd>
<dt class="hdlist1">topic:  id: 5</dt>
<dd>
<p>explanation</p>
</dd>
<dt class="hdlist1">topic: topic/ id: 1</dt>
<dd>
<p>explanation</p>
</dd>
<dt class="hdlist1">topic: topic/ id: 2</dt>
<dd>
<p>explanation</p>
</dd>
<dt class="hdlist1">topic: topic/ id: 3</dt>
<dd>
<p>explanation</p>
</dd>
<dt class="hdlist1">topic: topic/ id: 4</dt>
<dd>
<p>explanation</p>
</dd>
<dt class="hdlist1">topic: topic/ id: 5</dt>
<dd>
<p>explanation</p>
</dd>
</dl>
</div>`)
      })
    })
  })
})
