'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

const DEBUG = false

describe('index extension description list tests', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup()
    contentCatalog = r.contentCatalog
    file = r.file
  })

  function checkPair (pair) {
    expect(Array.isArray(pair)).to.equal(true)
    expect(pair.length).to.equal(2)
    const terms = pair[0]
    expect(Array.isArray(terms)).to.equal(true)
    expect(terms.length).to.equal(1)
    const term = terms[0]
    expect(term.context).to.equal('list_item')
    expect(pair[1].context).to.equal('list_item')
    return {
      term,
      def: pair[1],
    }
  }

  function checkDoc (doc, pre, count, marker, match, depth) {
    expect(doc).to.not.equal(null)
    var block = doc
    for (var i = 0; i < depth; i++) {
      DEBUG && console.log(`block context: ${block.context}, block id: ${block.$$id}, i: ${i}, block count: ${block.blocks.length}`)
      expect(block.blocks.length).to.eq(1)
      block = block.blocks[0]
      DEBUG && console.log(`sub block context: ${block.context}, block id: ${block.$$id}, i: ${i}, block count: ${block.blocks.length}`)
      expect(block.context).to.eq('dlist')
      expect(block.blocks).to.not.equal(null)
      if (i < depth - 1) {
        block = block.blocks[0]
        block = checkPair(block).def
      }
    }
    expect(block.blocks.length).to.equal(pre + count)
    block.blocks.forEach((pair, i) => {
      DEBUG && console.log(`pair ${i}`, typeof pair)
      if (i >= pre) {
        const term = checkPair(pair).term
        // expect(listitem.marker).to.equal(marker)
        expect(term.text).to.match(match)
      }
    })
  }

  ;[
    {
      page: (filter) => `indexDescriptionList::[${filter}$xref,description,style=horizontal]
    `,
      pre: 0,
      marker: '*',
      depth: 1,
      id: 1,
    },
    {
      page: (filter) => `[horizontal]
A list:: with descriptions

indexDescriptionList::[level=1,${filter}descAttribute=description]
`,
      pre: 1,
      marker: '*',
      depth: 1,
      id: 2,
    },
    {
      page: (filter) => `A Term:: a description

indexDescriptionList::[level=2,${filter}descAttribute=description]
`,
      pre: 0,
      marker: '**',
      depth: 2,
      id: 3,
    },
    {
      page: (filter) => `A list:: a def
A Sublist::: a subdef

indexDescriptionList::[level=2,${filter}descAttribute=description]
`,
      pre: 1,
      marker: '**',
      depth: 2,
      id: 4,
    },
    {
      page: (filter) => `A list:: a def
A Sublist::: a subdef

indexDescriptionList::[level=3,${filter}descAttribute=description]
`,
      pre: 0,
      marker: '***',
      depth: 3,
      id: 5,
    },
    {
      page: (filter) => `A list:: a def
A Sublist::: a subdef
A Subsublist:::: a subsubdef

indexDescriptionList::[level=2,${filter}descAttribute=description]
`,
      pre: 1,
      marker: '**',
      depth: 2,
      id: 6,
    },
    {
      page: (filter) => `A list:: a def
A Sublist::: a subdef
A Subsublist:::: a subsubdef

indexDescriptionList::[level=2,${filter}descAttribute=description]
`,
      pre: 1,
      marker: '**',
      depth: 2,
      id: 7,
    },
    {
      page: (filter) => `A list:: a def
A Sublist::: a subdef
A Subsublist:::: a subsubdef

indexDescriptionList::[level=1,${filter}descAttribute=description]
`,
      pre: 1,
      marker: '*',
      depth: 1,
      id: 8,
    },
    {
      page: (filter) => `indexDescriptionList::[${filter}=\`The Link: $\{$xref}\`,=\`*$\{mod2}* and $\{mod3}\`,style=horizontal]
    `,
      pre: 0,
      marker: '*',
      depth: 1,
      id: 1,
    },
  ].forEach(({ page, pre, marker, depth, id }) => {
    describe(`test id: ${id} pre: ${pre} marker: ${marker} depth: ${depth}`, () => {
      it('no filter test', () => {
        file.contents = Buffer.from(page(''))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 10, marker, /xref:1\.0@c1:ROOT:(topic\/)?page.\.adoc/, depth)
      })

      it('component * filter test', () => {
        file.contents = Buffer.from(page('component=*,'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 20, marker, /xref:1\.0@c.:ROOT:(topic\/)?page.\.adoc/, depth)
      })

      it('component c2 filter test', () => {
        file.contents = Buffer.from(page('component=c2,'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 10, marker, /xref:1\.0@c2:ROOT:(topic\/)?page.\.adoc/, depth)
      })

      it('version * filter test', () => {
        file.contents = Buffer.from(page('version=*,'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 20, marker, /xref:(1\.0|master)@c1:ROOT:(topic\/)?page.\.adoc/, depth)
      })

      it('version master filter test', () => {
        file.contents = Buffer.from(page('version=master,'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 10, marker, /xref:master@c1:ROOT:(topic\/)?page.\.adoc/, depth)
      })

      it('module * filter test', () => {
        file.contents = Buffer.from(page('module=*,'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 20, marker, /xref:1\.0@c1:(ROOT|module):(topic\/)?page.\.adoc/, depth)
      })

      it('module module filter test', () => {
        file.contents = Buffer.from(page('module=module,'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 10, marker, /xref:1\.0@c1:module:(topic\/)?page.\.adoc/, depth)
      })

      it('relative topic filter test', () => {
        file.contents = Buffer.from(page('relative=topic/**,'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 5, marker, /xref:1\.0@c1:ROOT:topic\/page.\.adoc/, depth)
      })
    })
  })
})
