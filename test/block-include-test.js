'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

describe('index extension block tests with include', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup(false, false)
    contentCatalog = r.contentCatalog
    ;['', 'topic/'].forEach((topic) => {
      ;[1, 2, 3, 4, 5].forEach((id) => {
        contentCatalog.addFile({
          origin: { site: r.primarySiteUrl },
          out: undefined,
          src: {
            component: 'c1',
            version: 'master',
            module: 'm1',
            family: 'partial',
            relative: `${topic}page${id}.adoc`,
          },
          contents: Buffer.from(`Included content: topic: ${topic} id: ${id}\n`),
          path: r.path,
        })
      })
    })
    file = r.file
  })

  function checkDoc (doc, count, match) {
    expect(doc).to.not.equal(null)
    expect(doc.blocks.length).to.eq(count)
    doc.blocks.forEach((block, i) => {
      expect(block.context).to.eq('document')
    })
  }

  ;[
    {
      page: (filter) => `

[indexBlock,'resourceid',family=partial,version=master,module=m1]
----
\\include::{resourceid}[]
----
`,
    },
  ].forEach(({ page, matches }) => {
    describe(`match: ${matches}`, () => {
      it('no filter test', () => {
        file.contents = Buffer.from(page(''))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, 10, matches)
        expect(doc.convert()).to.equal(`<div class="paragraph">
<p>Included content: topic:  id: 1</p>
</div>
<div class="paragraph">
<p>Included content: topic:  id: 2</p>
</div>
<div class="paragraph">
<p>Included content: topic:  id: 3</p>
</div>
<div class="paragraph">
<p>Included content: topic:  id: 4</p>
</div>
<div class="paragraph">
<p>Included content: topic:  id: 5</p>
</div>
<div class="paragraph">
<p>Included content: topic: topic/ id: 1</p>
</div>
<div class="paragraph">
<p>Included content: topic: topic/ id: 2</p>
</div>
<div class="paragraph">
<p>Included content: topic: topic/ id: 3</p>
</div>
<div class="paragraph">
<p>Included content: topic: topic/ id: 4</p>
</div>
<div class="paragraph">
<p>Included content: topic: topic/ id: 5</p>
</div>`)
      })
    })
  })
})
