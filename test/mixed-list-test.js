'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

describe('index extension mixed list tests', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup()
    contentCatalog = r.contentCatalog
    file = r.file
  })

  function checkPair (pair) {
    expect(Array.isArray(pair)).to.equal(true)
    expect(pair.length).to.equal(2)
    const terms = pair[0]
    expect(Array.isArray(terms)).to.equal(true)
    expect(terms.length).to.equal(1)
    const term = terms[0]
    expect(term.context).to.equal('list_item')
    expect(pair[1].context).to.equal('list_item')
    return {
      term,
      def: pair[1],
    }
  }

  function checkDoc (doc, pre, count, marker, match, depth, listTypes) {
    expect(doc).to.not.equal(null)
    var block = doc
    for (var i = 0; i < depth; i++) {
      expect(block.blocks.length).to.eq(1)
      block = block.blocks[0]
      expect(block.context).to.eq(listTypes[i])
      expect(block.blocks).to.not.equal(null)
      if (i < depth - 1) {
        block = block.blocks[0]
        if (listTypes[i] === 'dlist') {
          block = checkPair(block).def
        } else {
          expect(block.context).to.eq('list_item')
        }
      }
    }
    expect(block.blocks.length).to.equal(pre + count)
    block.blocks.forEach((listitem, i) => {
      if (i >= pre) {
        if (listTypes[depth - 1] === 'dlist') {
          const term = checkPair(listitem).term
          // expect(listitem.marker).to.equal(marker)
          expect(term.text).to.match(match)
        } else {
          expect(listitem.context).to.equal('list_item')
          expect(listitem.marker).to.equal(marker)
          expect(listitem.text).to.match(match)
        }
      }
    })
  }

  ;[
    {
      page: (filter) => `indexOrderedList::[${filter}]
    `,
      pre: 0,
      marker: '.',
      depth: 1,
      id: 1,
      listTypes: ['olist'],
    },
    {
      page: (filter) => `. A list

indexOrderedList::[level=1,${filter}]
`,
      pre: 1,
      marker: '.',
      depth: 1,
      id: 2,
      listTypes: ['olist'],
    },
    {
      page: (filter) => `. A list

indexList::[level=2,${filter}]
`,
      pre: 0,
      marker: '**',
      depth: 2,
      id: 3,
      listTypes: ['olist', 'ulist'],
    },
    {
      page: (filter) => `* A list
.. A Sublist

indexOrderedList::[level=2,${filter}]
`,
      pre: 1,
      marker: '..',
      depth: 2,
      id: 4,
      listTypes: ['ulist', 'olist'],
    },
    {
      page: (filter) => `* A list
.. A Sublist

indexDescriptionList::[level=3,${filter ? filter + ',' : ''}descAttr=description]
`,
      pre: 0,
      depth: 3,
      id: 5,
      listTypes: ['ulist', 'olist', 'dlist'],
    },
    {
      page: (filter) => `. A list
A Sublist:: description
... A Subsublist

indexDescriptionList::[level=2,${filter ? filter + ',' : ''}descAttr=description]
`,
      pre: 1,
      depth: 2,
      id: 6,
      listTypes: ['olist', 'dlist'],
    },
    {
      page: (filter) => `A Dlist:: Hi!
A SubDlist::: There!
... A Subsublist

indexDescriptionList::[level=2,${filter ? filter + ',' : ''}descAttr=description]
`,
      pre: 1,
      depth: 2,
      id: 7,
      listTypes: ['dlist', 'dlist'],
    },
    {
      page: (filter) => `* A list
A SubDlist:: foo
A SubsubDlist::: bar

indexList::[level=1,${filter}]
`,
      pre: 1,
      marker: '*',
      depth: 1,
      id: 8,
      listTypes: ['ulist'],
    },
  ].forEach(({ page, pre, marker, depth, id, listTypes }) => {
    describe(`Test id: ${id}, pre: ${pre} marker: ${marker} depth: ${depth}`, () => {
      it('no filter test', () => {
        file.contents = Buffer.from(page(''))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 10, marker, /xref:1\.0@c1:ROOT:(topic\/)?page.\.adoc/, depth, listTypes)
      })

      it('component * filter test', () => {
        file.contents = Buffer.from(page('component=*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 20, marker, /xref:1\.0@c.:ROOT:(topic\/)?page.\.adoc/, depth, listTypes)
      })

      it('component c2 filter test', () => {
        file.contents = Buffer.from(page('component=c2'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 10, marker, /xref:1\.0@c2:ROOT:(topic\/)?page.\.adoc/, depth, listTypes)
      })

      it('version * filter test', () => {
        file.contents = Buffer.from(page('version=*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 20, marker, /xref:(1\.0|master)@c1:ROOT:(topic\/)?page.\.adoc/, depth, listTypes)
      })

      it('version master filter test', () => {
        file.contents = Buffer.from(page('version=master'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 10, marker, /xref:master@c1:ROOT:(topic\/)?page.\.adoc/, depth, listTypes)
      })

      it('module * filter test', () => {
        file.contents = Buffer.from(page('module=*'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 20, marker, /xref:1\.0@c1:(ROOT|module):(topic\/)?page.\.adoc/, depth, listTypes)
      })

      it('module module filter test', () => {
        file.contents = Buffer.from(page('module=module'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 10, marker, /xref:1\.0@c1:module:(topic\/)?page.\.adoc/, depth, listTypes)
      })

      it('relative topic filter test', () => {
        file.contents = Buffer.from(page('relative=topic/**'))
        const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
        checkDoc(doc, pre, 5, marker, /xref:1\.0@c1:ROOT:topic\/page.\.adoc/, depth, listTypes)
      })
    })
  })
})
