'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

describe('index extension nested list tests', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup(false, true)
    contentCatalog = r.contentCatalog
    file = r.file
  })

  function checkDoc (node, counts, marker, match, listType) {
    expect(node).to.not.equal(null)
    expect(node.blocks.length).to.eq(1)
    const list = node.blocks[0]
    expect(list.context).to.eq(listType)
    expect(list.blocks).to.not.equal(null)
    expect(list.blocks.length).to.equal(counts[0])
    list.blocks.forEach((listitem, i) => {
      expect(listitem.context).to.equal('list_item')
      expect(listitem.marker).to.match(marker)
      expect(listitem.text).to.match(match[0])
      // console.log(listitem.text)
      if (listitem.blocks.length) {
        checkDoc(listitem, counts.slice(1), marker, match.slice(1), listType)
      }
    })
  }

  ;[
    {
      listType: 'ulist',
      listMacro: 'indexNestedList',
      markerChar: /\*/,
    },
    {
      listType: 'olist',
      listMacro: 'indexNestedOrderedList',
      markerChar: /\./,
    },
  ].forEach(({ listType, listMacro, markerChar }) => {
    ;[
      {
        page: (filter) => `${listMacro}::["l0,$xref"${filter}]
    `,
        marker: markerChar,
        id: 1,
        counts: [3, 45],
        match: [/I|II|III/, /xref:1\.0@nested:ROOT:pageI*\.[A-C]\.[1-3]\.[1-5]\.adoc/],
      },
      {
        page: (filter) => `${listMacro}::["l0,l1,$xref"${filter}]
    `,
        marker: markerChar,
        id: 2,
        counts: [3, 3, 15],
        match: [/I|II|III/, /[ABC]/, /xref:1\.0@nested:ROOT:pageI*\.[A-C]\.[1-3]\.[1-5]\.adoc/],
      },
      {
        page: (filter) => `${listMacro}::["l0,l1,l2,$xref"${filter}]
    `,
        marker: markerChar,
        id: 3,
        counts: [3, 3, 3, 5],
        match: [/I|II|III/, /[ABC]/, /[123]/, /xref:1\.0@nested:ROOT:pageI*\.[A-C]\.[1-3]\.[1-5]\.adoc/],
      },
      {
        page: (filter) => `${listMacro}::["l1,l2,l0,$xref"${filter}]
    `,
        marker: markerChar,
        id: 3,
        counts: [3, 3, 3, 5],
        match: [/[ABC]/, /[123]/, /I|II|III/, /xref:1\.0@nested:ROOT:pageI*\.[A-C]\.[1-3]\.[1-5]\.adoc/],
      },
    ].forEach(({ page, marker, id, counts, match }) => {
      describe(`List type: ${listType}, test id: ${id}, marker: ${marker}`, () => {
        it('component * filter test', () => {
          file.contents = Buffer.from(page(',component=*'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          // console.log(doc.convert())
          checkDoc(doc, counts, marker, match, listType)
        })
      })
    })
  })
})
