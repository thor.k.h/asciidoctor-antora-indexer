'use strict'
/* eslint-env mocha */

const { expect } = require('chai')

const setup = require('./setup')
const loadAsciidoc = require('@antora/asciidoc-loader')
const indexExtension = require('../lib/index-extension')

describe('index extension list tests', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup()
    contentCatalog = r.contentCatalog
    file = r.file
  })

  function checkDoc (doc, pre, count, marker, match, depth, listType) {
    expect(doc).to.not.equal(null)
    var block = doc
    for (var i = 0; i < depth; i++) {
      expect(block.blocks.length).to.eq(1)
      block = block.blocks[0]
      expect(block.context).to.eq(listType)
      expect(block.blocks).to.not.equal(null)
      if (i < depth - 1) {
        block = block.blocks[0]
        expect(block.context).to.eq('list_item')
      }
    }
    expect(block.blocks.length).to.equal(pre + count)
    block.blocks.forEach((listitem, i) => {
      expect(listitem.context).to.equal('list_item')
      expect(listitem.marker).to.equal(marker)
      if (i >= pre) {
        expect(listitem.text).to.match(match)
      }
    })
  }

  ;[
    {
      listType: 'ulist',
      listMacro: 'indexList',
      markerChar: '*',
    },
    {
      listType: 'olist',
      listMacro: 'indexOrderedList',
      markerChar: '.',
    },
  ].forEach(({ listType, listMacro, markerChar }) => {
    ;[
      {
        page: (filter) => `${listMacro}::[${filter}]
    `,
        pre: 0,
        marker: markerChar,
        depth: 1,
        id: 1,
      },
      {
        page: (filter) => `${markerChar} A list

${listMacro}::[level=1,${filter}]
`,
        pre: 1,
        marker: markerChar,
        depth: 1,
        id: 2,
      },
      {
        page: (filter) => `${markerChar} A list

${listMacro}::[level=2,${filter}]
`,
        pre: 0,
        marker: markerChar.repeat(2),
        depth: 2,
        id: 3,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist

${listMacro}::[level=2,${filter}]
`,
        pre: 1,
        marker: markerChar.repeat(2),
        depth: 2,
        id: 4,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist

${listMacro}::[level=3,${filter}]
`,
        pre: 0,
        marker: markerChar.repeat(3),
        depth: 3,
        id: 5,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist
${markerChar.repeat(3)} A Subsublist

${listMacro}::[level=2,${filter}]
`,
        pre: 1,
        marker: markerChar.repeat(2),
        depth: 2,
        id: 6,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist
${markerChar.repeat(3)} A Subsublist

${listMacro}::[level=2,${filter}]
`,
        pre: 1,
        marker: markerChar.repeat(2),
        depth: 2,
        id: 7,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist
${markerChar.repeat(3)} A Subsublist

${listMacro}::[level=1,${filter}]
`,
        pre: 1,
        marker: markerChar,
        depth: 1,
        id: 8,
      },
    ].forEach(({ page, pre, marker, depth, id }) => {
      describe(`List type: ${listType}, test id: ${id}, pre: ${pre} marker: ${marker} depth: ${depth}`, () => {
        it('no filter test', () => {
          file.contents = Buffer.from(page(''))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 10, marker, /xref:1\.0@c1:ROOT:(topic\/)?page.\.adoc/, depth, listType)
        })

        it('component * filter test', () => {
          file.contents = Buffer.from(page('component=*'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 20, marker, /xref:1\.0@c.:ROOT:(topic\/)?page.\.adoc/, depth, listType)
        })

        it('component c2 filter test', () => {
          file.contents = Buffer.from(page('component=c2'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 10, marker, /xref:1\.0@c2:ROOT:(topic\/)?page.\.adoc/, depth, listType)
        })

        it('version * filter test', () => {
          file.contents = Buffer.from(page('version=*'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 20, marker, /xref:(1\.0|master)@c1:ROOT:(topic\/)?page.\.adoc/, depth, listType)
        })

        it('version master filter test', () => {
          file.contents = Buffer.from(page('version=master'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 10, marker, /xref:master@c1:ROOT:(topic\/)?page.\.adoc/, depth, listType)
        })

        it('module * filter test', () => {
          file.contents = Buffer.from(page('module=*'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 20, marker, /xref:1\.0@c1:(ROOT|module):(topic\/)?page.\.adoc/, depth, listType)
        })

        it('module module filter test', () => {
          file.contents = Buffer.from(page('module=module'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 10, marker, /xref:1\.0@c1:module:(topic\/)?page.\.adoc/, depth, listType)
        })

        it('relative topic filter test', () => {
          file.contents = Buffer.from(page('relative=topic/**'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 5, marker, /xref:1\.0@c1:ROOT:topic\/page.\.adoc/, depth, listType)
        })

        it('relative and relexclude topic filter test', () => {
          file.contents = Buffer.from(page('relative=topic/**, exclude=topic/page2.adoc'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 4, marker, /xref:1\.0@c1:ROOT:topic\/page.\.adoc/, depth, listType)
        })

        it('relative and relexclude 2 clause topic filter test', () => {
          file.contents = Buffer.from(page('relative="topic/**, page3.adoc ,page5.adoc", exclude="topic/page2.adoc, topic/page4.adoc"'))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          checkDoc(doc, pre, 5, marker, /xref:1\.0@c1:ROOT:(topic\/)?page.\.adoc/, depth, listType)
        })
      })
    })
  })
})

describe('index extension list render tests', () => {
  const asciidocConfig = { extensions: [indexExtension] }
  var contentCatalog
  var file

  beforeEach(() => {
    const r = setup(true, false, '__')
    contentCatalog = r.contentCatalog
    file = r.file
  })

  ;[
    {
      listType: 'ulist',
      listMacro: 'indexList',
      markerChar: '*',
    },
    {
      listType: 'olist',
      listMacro: 'indexOrderedList',
      markerChar: '.',
    },
  ].forEach(({ listType, listMacro, markerChar }) => {
    ;[
      {
        page: (filter) => `${listMacro}::[${filter}]
    `,
        pre: 0,
        marker: markerChar,
        depth: 1,
        id: 1,
      },
      {
        page: (filter) => `${markerChar} A list

${listMacro}::[level=1,${filter}]
`,
        pre: 1,
        marker: markerChar,
        depth: 1,
        id: 2,
      },
      {
        page: (filter) => `${markerChar} A list

${listMacro}::[level=2,${filter}]
`,
        pre: 0,
        marker: markerChar.repeat(2),
        depth: 2,
        id: 3,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist

${listMacro}::[level=2,${filter}]
`,
        pre: 1,
        marker: markerChar.repeat(2),
        depth: 2,
        id: 4,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist

${listMacro}::[level=3,${filter}]
`,
        pre: 0,
        marker: markerChar.repeat(3),
        depth: 3,
        id: 5,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist
${markerChar.repeat(3)} A Subsublist

${listMacro}::[level=2,${filter}]
`,
        pre: 1,
        marker: markerChar.repeat(2),
        depth: 2,
        id: 6,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist
${markerChar.repeat(3)} A Subsublist

${listMacro}::[level=2,${filter}]
`,
        pre: 1,
        marker: markerChar.repeat(2),
        depth: 2,
        id: 7,
      },
      {
        page: (filter) => `${markerChar} A list
${markerChar.repeat(2)} A Sublist
${markerChar.repeat(3)} A Subsublist

${listMacro}::[level=1,${filter}]
`,
        pre: 1,
        marker: markerChar,
        depth: 1,
        id: 8,
      },
    ].forEach(({ page, pre, marker, depth, id }) => {
      describe(`List type: ${listType}, test id: ${id}, pre: ${pre} marker: ${marker} depth: ${depth}`, () => {
        it('no filter test', () => {
          file.contents = Buffer.from(page(''))
          const doc = loadAsciidoc(file, contentCatalog, asciidocConfig)
          // checkDoc(doc, pre, 10, marker, /xref:1\.0@c1:ROOT:(topic\/)?page.\.adoc/, depth, listType)
          const html = doc.convert()
          console.log('html', html)
        })
      })
    })
  })
})
