# Antora Index Asciidoctor Extension
:version: 0.0.8

This is an Asciidoctor extension for Antora, having six blockMacro processors, two inlineMacro processors, and one block processor.

* indexList blockMacro processor produces an unordered list of, by default, xrefs to selected content catalog pages.
* indexOrderedList blockMacro processor produces an ordered list of, by default, xrefs to selected content catalog pages.
* indexDescriptionList blockMacro processor produces a description list of, by default, xrefs to selected content catalog pages, with the "description" displaying a configured target page attribute.
* indexNestedList blockMacro processor produces an unordered nested list of, by default, xrefs to selected content catalog pages.
* indexNestedOrderedList blockMacro processor produces an ordered nested list, by default, of xrefs to selected content catalog pages.
* indexTable blockMacro processor adds to a preceding table rows constructed from xrefs to content catalog pages and attributes of those pages.
* indexCount inlineMacro processor counts the pages selected by the criteria.
* indexUniqueCount inlineMacro processor counts the unique values of the specified attribute in the pages selected by the criteria.
* indexBlock block processor uses the supplied block as a template, repeating for each selected page, and supplying the selected page's header attributes as "variables" to the template.

One way to view the philosophy behind this extension is that the combination of the Antora project structure and page attributes form a database, so it is natural to query this database.
This extension provides some query facilities.
Perhaps it is most likely to be useful in sections that consist of non-sequential pages documenting equivalent features.
For example if a system consists of a core framework together with a number of extensions, this could index the extensions.

The git repository for this extension is https://gitlab.com/djencks/asciidoctor-antora-indexer.

For reasonably complete instructions, consult https://gitlab.com/djencks/asciidoctor-antora-indexer.
Due to the limitations of markdown, there is no attempt to provide details here.

## Installation

Available through npm as @djencks/asciidoctor-antora-indexer.

The simplest way to install this is through a package.json file next to your Antora playbook.

Configure this as an asciidoctor extension in your playbook.

## Configuration

### Common Configuration of selected pages

All processors are configured similarly to select pages to index.
All selected pages are, by default, ordered by case-insensitive doctitle.
The current page is always excluded from the list of indexed pages.
All macro processors use the "short" form in which the macro name is immediately followed by configuration in square brackets.


### indexList, indexOrderedList, indexDescriptionList, indexNestedList, and indexNestedOrderedList Block Macro Processors

These block macro processors are used freestanding or in an existing list or sublist.

### indexTable Block Macro Processor

This block processor is used to add rows to a table whose cell contents are attribute values of the selected pages.

### indexCount Inline Macro Processor

This inline processor counts the pages matching the criteria.
There are no parameters beyond the filter.

### indexUniqueCount Inline Macro Processor

This inline macro processor counts the unique values of the specified attribute in the pages matching the criteria.

### indexBlock block processor

This block processor uses the block as a template and reproduces it for each selected page.

## Examples

A small example project is in the link:https://gitlab.com/djencks/simple-examples[Simple Examples] project at extensions/index-extension.
Another project, index-extension-tags, shows how to use this for 'blog-like' tags functionality.
