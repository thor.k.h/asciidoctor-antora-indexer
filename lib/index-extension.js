'use strict'

const picomatch = require('picomatch')
const Opal = global.Opal
const PreprocessorReader = Opal.module(null, 'Asciidoctor').PreprocessorReader
const parse = require('esprima').parse
const staticEval = require('static-eval')
const camelcaseKeys = require('camelcase-keys')

const { computeParent, newList, print } = require('./list-utils')
const { doTemplateDoc, doTemplate } = require('./block')

const UNESCAPE_STAR = /\\\*/g

const DEBUG = false

module.exports.register = function (registry, { file, contentCatalog }) {
  function processBlocks (attributes, template, parent, formats) {
    doFiles(attributes, (item) => {
      const innerDoc = doTemplateDoc(registry, template, parent, attributes, formats, item)
      parent.blocks.push(innerDoc)
    })
  }

  function processTemplates (attributes, template, parent, formats, self) {
    const lines = []
    doFiles(attributes, (item) => {
      lines.push(...doTemplate(registry, template, parent, attributes, formats, item))
    })
    const reader = PreprocessorReader.$new(parent.document, lines)
    self.parseContent(parent, reader, Opal.hash2(Object.keys(attributes), attributes))
  }

  function indexBlock (name, processFiles) {
    return function () {
      const self = this
      self.named(name)
      self.onContext(['listing', 'open', 'paragraph'])
      self.positionalAttributes(['formats'])
      self.process(function (parent, reader, attributes) {
        const template = reader.$read()

        const formatSpecs = attributes.formats ? attributes.formats.split(',') : []
        const formats = formatSpecs.reduce((accum, formatSpec) => {
          if (formatSpec === 'xref') {
            accum[formatSpec] = (item) => xref(item)
          } else if (formatSpec === 'resourceid') {
            accum[formatSpec] = (item) => resourceId(item)
          } else if (formatSpec.includes('=')) {
            const pos = formatSpec.indexOf('=')
            const name = formatSpec.slice(0, pos)
            accum[name] = createEvalFn(formatSpec.slice(pos + 1))
          } else {
            accum[formatSpec] = (item) => item[1][formatSpec]
          }
          return accum
        }, {})
        processFiles(attributes, template, parent, formats, self)
      })
    }
  }

  function listProcessor (name, type, markerChar) {
    return function () {
      const self = this
      self.named(name)
      self.$option('format', 'short')
      self.positionalAttributes(['format'])
      self.process((parent, target, attrs) => {
        // console.log('attrs', attrs)
        DEBUG && print(parent.document)
        const logLists = parent.document.hasAttribute('antora-indexer-log-lists')
        const depth = attrs.level ? Number.parseInt(attrs.level) : undefined
        const evalFn = attrs.format ? createEvalFn(attrs.format) : (item) => xref(item)
        const list = computeParent(parent, depth, self, type, attrs.style)
        const marker = markerChar.repeat(depth || 1)
        if (logLists) {
          console.log(`${type} with attributes `, attrs)
        }

        doFiles(attrs, (item) => {
          // console.log(`list item`, item.src)
          const listItem = self.createListItem(list, evalFn(item))
          listItem.marker = marker
          list.blocks.push(listItem)
          if (logLists) {
            console.log(`${marker} ${listItem.text}`)
          }
        })
        DEBUG && print(parent.document)
      })
    }
  }

  function nestedListProcessor (name, type, markerChar) {
    return function () {
      const self = this
      self.named(name)
      self.$option('format', 'short')
      self.positionalAttributes(['formats'])
      self.process((parent, target, attrs) => {
        // console.log('attrs', attrs)
        DEBUG && print(parent.document)
        const logLists = parent.document.hasAttribute('antora-indexer-log-lists')
        const formatSpecs = attrs.formats.split(',')
        const formats = formatSpecs.map((formatSpec) => {
          return createFormatFn(formatSpec)
        })

        const lists = [newList(type, parent, self, attrs.style)]
        lists.length = formats.length
        if (logLists) {
          console.log(`${type} with attributes `, attrs)
        }

        let state = []
        state.length = formats.length
        doFiles(attrs, (item) => {
          const newState = format(item, formats)
          let match = true
          for (let i = 0; i < formats.length; i++) {
            match = match && state[i] === newState[i]
            if (!match || i === formats.length - 1) {
              const listItem = self.createListItem(lists[i], formats[i](item))
              const marker = markerChar.repeat(i + 1)
              listItem.marker = marker
              lists[i].blocks.push(listItem)
              if (logLists) {
                console.log(`${marker} ${listItem.text}`)
              }
              if (i < formats.length - 1) {
                lists[i + 1] = newList(type, lists[i].blocks[lists[i].blocks.length - 1], self, attrs.style)
              }
            }
          }
          state = newState
        },
        multiCompare(formats))
        DEBUG && print(parent.document)
      })
    }
  }

  function format (item, formats) {
    return formats.map((format) => format(item))
  }

  function indexDescriptionList () {
    const self = this
    self.named('indexDescriptionList')
    self.$option('format', 'short')
    self.positionalAttributes(['termAttribute', 'descAttribute'])
    self.process((parent, target, attrs) => {
      // console.log('attrs', attrs)
      DEBUG && print(parent.document)
      const depth = attrs.level ? Number.parseInt(attrs.level) : undefined
      const list = computeParent(parent, depth, self, 'dlist', attrs.style)
      // const marker = '*'.repeat(attrs.level || 1)
      const termFn = createFormatFn(attrs.termAttribute || '$xref')
      const descFn = createFormatFn(attrs.descAttribute || '')

      doFiles(attrs, (item) => {
        // console.log(`list item`, item.src)
        const listTerm = self.createListItem(list, termFn(item))
        const listItem = self.createListItem(list, descFn(item))
        // listItem.marker = marker
        list.blocks.push([[listTerm], listItem])
      })
      DEBUG && print(parent.document)
    })
  }

  function indexTable () {
    const self = this
    self.named('indexTable')
    self.$option('format', 'short')
    self.process((parent, target, attrs) => {
      // console.log('attrs', attrs)

      const table = parent.blocks[parent.blocks.length - 1]
      if (table.context !== 'table') {
        console.warn(`indexTable block macro must follow a table, not ${table.context}`)
        return
      }
      const columns = table.columns
      const rows = table.rows.body
      const cellAttrs = attrs.cells ? attrs.cells.split(',') : []
      if (columns.length !== cellAttrs.length) {
        console.warn(`column count ${columns.length} differs from 'cells' attribute count ${cellAttrs.length}`)
        cellAttrs.length = columns.length
      }
      const formats = cellAttrs.map((formatSpec) => {
        return createFormatFn(formatSpec)
      })

      doFiles(attrs, (item) => {
        const row = columns.map((col, i) =>
          Opal.Asciidoctor.Table.Cell.$new(col, formats[i](item)))
        rows.push(row)
      })
    })
  }

  function indexCount () {
    const self = this
    self.named('indexCount')
    self.$option('format', 'short')
    self.process((parent, target, attrs) => {
      fixAttrs(attrs)
      var count = 0
      doFiles(attrs, (item) => count++)
      return self.createInline(parent, 'quoted', count)
    })
  }

  function indexUniqueCount () {
    const self = this
    self.named('indexUniqueCount')
    self.$option('format', 'short')
    self.process((parent, target, attrs) => {
      const unique = attrs.unique
      if (!unique) {
        console.warn('attribute \'unique\' must be specified')
        return 'attribute \'unique\' must be specified'
      }
      fixAttrs(attrs)
      var values = new Set()
      doFiles(attrs, (item) => values.add(item[1][unique]))
      return self.createInline(parent, 'quoted', values.size)
    })
  }

  function fixAttrs (attrs) {
    for (const key in attrs) {
      attrs[key] = attrs[key].replace(UNESCAPE_STAR, '*')
    }
  }

  function doFiles (attrs, content, compareFn = compare) {
    const { filter, relCompare, attributesPresent, attributesMissing, attributeValues } = getFilter(attrs)
    const files = contentCatalog.findBy(filter)
      .filter((test) => (filter.family ||
        (test.src.family === 'page' && test !== file) ||
        (test.src.family === 'alias' && test.rel !== file)) &&
        test.src.component && //never include site start page alias
        relCompare(test.src.relative))
      .map((item) => [item, attributes(item)])
      .filter((item) =>
        attributesPresent.every((attributeName) => attributeName in item[1]) &&
        attributesMissing.every((attributeName) => !(attributeName in item[1])) &&
        attributeValues.every(([name, value]) => item[1][name] === value)
      )
    files.sort(compareFn)

    files.forEach(content)
  }

  function getFilter (attrs) {
    const { component, version, module } = file.src
    const filter = {}
    attrs.component === '*' || (filter.component = attrs.component || component)
    attrs.version === '*' || (filter.version = attrs.version || version)
    attrs.module === '*' || (filter.module = attrs.module || module)
    attrs.family && (filter.family = attrs.family)
    const relCompare = attrs.relative ? picomatcher(attrs.relative, attrs.exclude) : () => true
    const attributesPresent = []
    const attributesMissing = []
    const attributeValues = []
    attrs.attributes && attrs.attributes.split(',').forEach((clause) => {
      const match = clause.split('=')
      if (match.length === 2) {
        attributeValues.push([match[0], match[1]])
      } else if (match.length === 1) {
        if (match[0].startsWith('!')) {
          attributesMissing.push(match[0].slice(1))
        } else {
          attributesPresent.push(match[0])
        }
      } else {
        console.warn(`invalid clause ${clause}`)
      }
    })
    return { filter, relCompare, attributesPresent, attributesMissing, attributeValues }
  }

  function picomatcher (relative, exclude) {
    const options = exclude ? { ignore: exclude.split(',').map((term) => term.trim()) } : {}
    return picomatch(relative.split(',').map((term) => term.trim()), options)
  }

  function doRegister (registry) {
    if (typeof registry.block === 'function') {
      registry.block(indexBlock('indexBlock', processBlocks))
      registry.block(indexBlock('indexTemplate', processTemplates))
    } else {
      console.warn('no \'block\' method on alleged registry')
    }
    if (typeof registry.blockMacro === 'function') {
      registry.blockMacro(indexTable)
      registry.blockMacro(indexDescriptionList)
      registry.blockMacro(listProcessor('indexList', 'ulist', '*'))
      registry.blockMacro(listProcessor('indexOrderedList', 'olist', '.'))
      registry.blockMacro(nestedListProcessor('indexNestedList', 'ulist', '*'))
      registry.blockMacro(nestedListProcessor('indexNestedOrderedList', 'olist', '.'))
    } else {
      console.warn('no \'blockMacro\' method on alleged registry')
    }
    if (typeof registry.inlineMacro === 'function') {
      registry.inlineMacro(indexCount)
      registry.inlineMacro(indexUniqueCount)
    } else {
      console.warn('no \'inlineMacro\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}

function createFormatFn (formatSpec) {
  if (formatSpec === '$xref') {
    return (item) => xref(item)
  } else if (formatSpec.startsWith('=')) {
    return createEvalFn(formatSpec.slice(1))
  } else {
    return (item) => item[1][formatSpec] || ''
  }
}

function createEvalFn (exp) {
  const parsed = parse(exp).body[0].expression
  return (item) => staticEval(parsed, Object.assign({
    $xref: xref(item),
    $resourceId: resourceId(item),
  }, camelcaseKeys(item[1])))
}

const compare = (f1, f2) => {
  const t1 = f1[1].doctitle.toLowerCase()
  const t2 = f2[1].doctitle.toLowerCase()
  return t1 < t2 ? -1 : t1 > t2 ? 1 : 0
}

function multiCompare (formats) {
  return (f1, f2) => {
    for (const format of formats) {
      const t1 = format(f1).toLowerCase()
      const t2 = format(f2).toLowerCase()
      if (t1 !== t2) {
        return t1 < t2 ? -1 : 1
      }
    }
    return 0
  }
}

function escapeRelative (relative) {
  // const orig = relative
  relative = relative.replace(/(__[^_]+(_[^_]+?)*__)/g, '\\$1')
  relative = relative.replace(/(\*\*[^*]+(\*[^*]+?)*\*\*)/g, '\\$1')
  // console.log(`${orig} >> ${relative}`)
  return relative
}

function xref (item) {
  const { component, version, module, relative } = item[0].src
  return `xref:${version}@${component}:${module}:${escapeRelative(relative)}[]`
}

function resourceId (item) {
  const { component, version, module, family, relative } = item[0].src
  return `${version}@${component}:${module}:${family}$${relative}`
}

function attributes (file) {
  file = file.src.family === 'alias' ? file.rel : file
  return Object.assign({ doctitle: file.src.relative }, file.asciidoc ? file.asciidoc.attributes : {})
}
